from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet,UnsupervisedDataSet
from pybrain.structure import LinearLayer

import sys;
import random;

ds = SupervisedDataSet(100, 1);
param_list = [];
ans_list = [];

extinct = [];
living = [];

with open('final_pop_1000.csv') as f:
	line = f.readline();
	while True:
		line = f.readline().strip('\n');
		if not line:
			break;

		params = line.split(',');
		param_list.append( params[:-1] );
		ans_list.append( params[-1] );
		if (int(params[-1]) == 1):
			extinct.append( params );
		else:
			living.append( params );

l = len(extinct);
living = random.sample(living, l)

#take equal amounts of extinct and living
print l;
for i in range(l):
	ds.addSample(map(float, extinct[i][:-1]), int(extinct[i][-1]));
	ds.addSample(map(float, living[i][:-1]), int(living[i][-1]));

net = buildNetwork(100, 50, 1, outclass=LinearLayer, bias=True, recurrent=True);

trainer = BackpropTrainer(net, ds);
trainer.trainEpochs(8);

ts = UnsupervisedDataSet(100);

for i in range(len(param_list)):
	ts.addSample(map(float, param_list[i]));

ans = net.activateOnDataset(ts);

truePos = 0;
trueNeg = 0;
falsePos = 0;
falseNeg = 0;

for i in range(len(ans)):
	#might need to round based on percent of feature value
	val = int(round(ans[i][0]));
	print val, ans_list[i];
	realAns = int(ans_list[i]);
	if val == realAns:
		if val == 0:
			trueNeg += 1;
		else:
			truePos += 1;
	else:
		if val == 0 and realAns == 1:
			falsePos += 1;
		else:
			falseNeg += 1;

print "TruePos: ", truePos;
print "TrueNeg: ", trueNeg;
print "FalsePos: ", falsePos;
print "FalseNeg: ", falseNeg;

#[ int(round(i)) for i in net.activateOnDataset(ts)[0] ]
