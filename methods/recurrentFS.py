from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet,UnsupervisedDataSet
from pybrain.structure import LinearLayer

import random;
import pickle;

features = [2,3,4,9,10,12,13,14,17,22,25,26,29,31,32,33,34,36,39,40,44,46,48,52,53,58,61,62,63,65,66,73,75,80,81,83,89,94,95,96,97,99];
features = [ x-1 for x in features ];

SIZE = len(features);

ds = SupervisedDataSet(SIZE, 1);
param_list = [];
ans_list = [];

with open('run4.csv') as f:
	line = f.readline();
	while True:
		line = f.readline().strip('\n');
		if not line:
			break;

		params = line.split(',');

		param_list.append( params[:-1] );
		ans_list.append( params[-1] );

with open('run5.csv') as f:
	line = f.readline();
	while True:
		line = f.readline().strip('\n');
		if not line:
			break;

		params = line.split(',');

		param_list.append( params[:-1] );
		ans_list.append( params[-1] );

#choose the features
param_list = map(lambda (x): [ x[i] for i in features ], param_list);

fileObject = open('recurrentFS.net', 'r');
net = pickle.load(fileObject);

ts = UnsupervisedDataSet(SIZE);

for i in range(len(param_list)):
	ts.addSample(map(float, param_list[i]));

ans = net.activateOnDataset(ts);

truePos = 0;
trueNeg = 0;
falsePos = 0;
falseNeg = 0;

posCount = 0;
negCount = 0;

for i in range(len(ans)):

	val = int(round(ans[i][0]));
	realAns = int(ans_list[i]);

	if realAns == 1:
		posCount += 1
	else:
		negCount += 1;

	if val == realAns:
		if val == 0:
			trueNeg += 1;
		else:
			truePos += 1;
	else:
		if val == 0 and realAns == 1:
			falsePos += 1;
		else:
			falseNeg += 1;

print "PosCount:", posCount;
print "NegCount:", negCount;

print "TruePos: ", truePos;
print "TrueNeg: ", trueNeg;
print "FalsePos: ", falsePos;
print "FalseNeg: ", falseNeg;

#[ int(round(i)) for i in net.activateOnDataset(ts)[0] ]
