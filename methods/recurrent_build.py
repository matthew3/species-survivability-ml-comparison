from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet,UnsupervisedDataSet
from pybrain.structure import LinearLayer
from pybrain.tools.validation import CrossValidator

import random;
import pickle;

features = range(100);

ds = SupervisedDataSet(len(features), 1);

param_list = [];
ans_list = [];

extinct = [];
living = [];

with open('run2.csv') as f:
	line = f.readline();
	while True:
		line = f.readline().strip('\n');
		if not line:
			break;

		params = line.split(',');
		param_list.append( params[:-1] );
		ans_list.append( params[-1] );
		if (int(params[-1]) == 1):
			extinct.append( params );
		else:
			living.append( params );

l = len(extinct);
living = random.sample(living, l);

#feature selection
living = map(lambda (x): [ x[i] for i in features ], living);
extinct = map(lambda (x): [ x[i] for i in features ], extinct);
param_list = map(lambda (x): [ x[i] for i in features ], param_list);

#take equal amounts of extinct and living
for i in range(l):
	ds.addSample(map(float, extinct[i]), 1);
	ds.addSample(map(float, living[i]), 0);

net = buildNetwork(len(features), 85, 1, outclass=LinearLayer, bias=True, recurrent=True);

trainer = BackpropTrainer(net, ds);
trainer.trainEpochs(1);

results = CrossValidator(trainer, ds, n_folds=10).validate(); 
print "Performance: ", results;

fileObject = open('recurrent.net', 'w');
pickle.dump(net, fileObject);
