from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet,UnsupervisedDataSet
from pybrain.structure import LinearLayer

import sys;
import random;

features = [2,3,4,9,10,12,13,14,17,22,25,26,29,31,32,33,34,36,39,40,44,46,48,52,53,58,61,62,63,65,66,73,75,80,81,83,89,94,95,96,97,99];

ds = SupervisedDataSet(len(features), 1);
param_list = [];
ans_list = [];

#features = map(lambda (x): x, features);

print len(features), " features";

extinct = [];
living = [];

with open('final_pop_1000.csv') as f:
	line = f.readline();
	while True:
		line = f.readline().strip('\n');
		if not line:
			break;

		params = line.split(',');
		param_list.append( params[:-1] );
		ans_list.append( params[-1] );
		if (int(params[-1]) == 1):
			extinct.append( params );
		else:
			living.append( params );

l = len(extinct);
living = random.sample(living, l);

#feature selection
living = map(lambda (x): [ x[i] for i in features ], living);
extinct = map(lambda (x): [ x[i] for i in features ], extinct);
param_list = map(lambda (x): [ x[i] for i in features ], param_list);

#take equal amounts of extinct and living
for i in range(l):
	ds.addSample(map(float, extinct[i]), 1);
	ds.addSample(map(float, living[i]), 0);

net = buildNetwork(len(features), 20, 1, outclass=LinearLayer, bias=True, recurrent=True);

trainer = BackpropTrainer(net, ds);
trainer.trainEpochs(2);

ts = UnsupervisedDataSet(len(features));

for i in range(len(param_list)):
	ts.addSample(map(float, param_list[i]));

ans = net.activateOnDataset(ts);

truePos = 0;
trueNeg = 0;
falsePos = 0;
falseNeg = 0;

for i in range(len(ans)):
	#might need to round based on percent of feature value
	val = int(round(ans[i][0]));
	print val, ans_list[i];
	realAns = int(ans_list[i]);
	if val == realAns:
		if val == 0:
			trueNeg += 1;
		else:
			truePos += 1;
	else:
		if val == 0 and realAns == 1:
			falsePos += 1;
		else:
			falseNeg += 1;

print "TruePos: ", truePos;
print "TrueNeg: ", trueNeg;
print "FalsePos: ", falsePos;
print "FalseNeg: ", falseNeg;

#[ int(round(i)) for i in net.activateOnDataset(ts)[0] ]
