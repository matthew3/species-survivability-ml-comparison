recurrent - 100 features
--------------------------
Performance: 
0.0

Confusion Matrix:
TruePos:  599
TrueNeg:  36880
FalsePos:  544
FalseNeg:  20171

recurrentFS1 - 42 features
--------------------------
Performance:
0.0

Confusion Matrix:
TruePos:  587
TrueNeg:  43711
FalsePos:  559
FalseNeg:  13337

recurrentFS2 - 8 features
--------------------------
Performance: 
0.0

Confusion Matrix:
TruePos:  1166
TrueNeg:  0
FalsePos:  0
FalseNeg:  57028
