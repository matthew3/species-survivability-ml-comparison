# README #

### What is this repository for? ###

This repository is a machine learning project for the University of Windsor's 4th year machine learning course. It aims to compare various machine learning techniques on simulated species survival data for survival prediction.
